<?php

namespace App\Http\Controllers;

use App\Report\Report;
use App\Report\ReportPractitionerDetail;
use App\Report\ReportPrimaryPractitionerDetail;
use App\Report\ReportPatientDetail;
use App\Report\ReportMedicineDetail;
use App\Report\ReportReactionDetail;
use App\Report\ReportOtherDetail;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use DB;

class IncidentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->report = new Report();
        $this->reportPractitionerDetail = new ReportPractitionerDetail();
        $this->reportPrimaryPractitionerDetail = new ReportPrimaryPractitionerDetail();
        $this->reportPatientDetail = new ReportPatientDetail();
        $this->reportMedicineDetail = new ReportMedicineDetail();
        $this->reportReactionDetail = new ReportReactionDetail();
        $this->reportOtherDetail = new ReportOtherDetail();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('incident_report_create');
    }

    public function postIncidentReportForm(Request $request)
    {
        
        $input = $request->all();       

        //die();

        $userId = Auth::id();

        /* Create report number */
        $lastId = $this->report->select('id')->orderBy('id', 'desc')->get()->first();
        $lastReportId = (is_null($lastId)) ? 0 : $lastId->id;
        $reportNumber = "IR".sprintf("%06d", ($lastReportId + 1));
        /* Create report number*/

        /* Report Array */
        $reportArray = [
            'report_number' => $reportNumber,
            'added_by'      => $input['addedBy'],
            'user_id'       => $userId
        ];

        // echo '<pre>';
        // print_r($reportArray);
        // echo '</pre>';

        /* Report Practitioner Detail Array */
        $reportPractitionerDetailArray = [
            'title'                     => $input['title'], 
            'first_name'                => $input['firstName'],
            'last_name'                 => $input['lastName'], 
            // 'medical_clinic'            => $input['medicalClinic'], 
            'medical_clinic_name'       => $input['medicalClinicName'], 
            // 'hospital'                  => $input['hospital'], 
            'hospital_name'             => $input['hospitalName'], 
            'hospital_unit_details'     => $input['hospitalUnitDetails'], 
            'email'                     => $input['email'], 
            'address_line_one'          => $input['address'], 
            'address_line_two'          => $input['address2'], 
            'suburb'                    => $input['suburb'],  
            'state'                     => $input['state'],
            'postcode'                  => $input['postcode'],
            'country'                   => $input['country'],
            'primary_practitioner'      => $input['isPrimaryPractitioner']
        ];

        if (array_key_exists('medicalClinic', $input) == true) :
            $reportPractitionerDetailArray = array_add($reportPractitionerDetailArray, 'medical_clinic', $input['medicalClinic']);
        endif;

        if (array_key_exists('hospital', $input) == true) :
            $reportPractitionerDetailArray = array_add($reportPractitionerDetailArray, 'hospital', $input['hospital']);
        endif;

        // echo '<pre>';
        // print_r($reportPractitionerDetailArray);
        // echo '</pre>';

        /* Report Primary Practitioner Detail Array */
        if ($input['isPrimaryPractitioner'] == 1) :
            $reportPrimaryPractitionerDetailArray = [
                'title'                     => $input['ppTitle'], 
                'first_name'                => $input['ppFirstName'],
                'last_name'                 => $input['ppLastName'], 
                // 'medical_clinic'            => $input['ppMedicalClinic'], 
                'medical_clinic_name'       => $input['ppMedicalClinicName'], 
                // 'hospital'                  => $input['ppHospital'], 
                'hospital_name'             => $input['ppHospitalName'], 
                'hospital_unit_details'     => $input['ppHospitalUnitDetails'], 
                'email'                     => $input['ppEmail'], 
                'address_line_one'          => $input['ppAddress'], 
                'address_line_two'          => $input['ppAddress2'], 
                'suburb'                    => $input['ppSuburb'],  
                'state'                     => $input['ppState'],
                'postcode'                  => $input['ppPostcode'],
                'country'                   => $input['ppCountry']
            ];

            if (array_key_exists('ppMedicalClinic', $input) == true) :
                $reportPrimaryPractitionerDetailArray = array_add($reportPrimaryPractitionerDetailArray, 'medical_clinic', $input['ppMedicalClinic']);
            endif;
    
            if (array_key_exists('ppHospital', $input) == true) :
                $reportPrimaryPractitionerDetailArray = array_add($reportPrimaryPractitionerDetailArray, 'hospital', $input['ppHospital']);
            endif;

            // echo '<pre>';
            // print_r($reportPrimaryPractitionerDetailArray);
            // echo '</pre>';
        endif;        

        $reportPatientDetailArray = [
            'patient_identification_number'         => $input['patientIdentificationNumber'], 
            'patient_initials'                      => $input['patientInitials'], 
            // 'ethnicity'                             => $input['ethnicity'], 
            // 'sex'                                   => $input['sex'], 
            // 'date_of_birth'                         => $DOB, 
            'weight'                                => $input['weight'], 
            'patient_symptoms'                      => $input['symptoms'], 
            'patient_diagnosis'                     => $input['diagnosis'], 
            'genetic_factors'                       => $input['genetic'], 
            'vitals_weight'                         => $input['patientWeight'], 
            'vitals_temp'                           => $input['patientTemp'], 
            'vitals_bp'                             => $input['patientBP'], 
            'vitals_hr'                             => $input['patientHR'], 
            'vitals_respiratory_rate'               => $input['patientRespiratoryRate'],       
            'vitals_oxygenation'                    => $input['patientOxygenation'], 
            'vitals_pain'                           => $input['patientPain'], 
            'vitals_appetite'                       => $input['patientAppetite'], 
            'vitals_urination'                      => $input['patientUrination'], 
            'vitals_defecation'                     => $input['patientDefecation'], 
            'vitals_vomiting'                       => $input['patientVomiting'], 
            'vitals_water_intake'                   => $input['patientWaterIntake'], 
            'vitals_food_intake'                    => $input['patientFoodIntake'], 
            'vitals_last_medication_time'           => $input['patientLastMedicationTime'], 
            'vitals_last_medication_dosage'         => $input['patientLastMedicationDosage'], 
            'vitals_last_medication_name'           => $input['patientLastMedicationName'], 
            'pathology'                             => $input['pathology']
        ];

        if (isset($input['dateOfBirth'])) :
            $dateofBirth = str_replace('/', '-', $input['dateOfBirth']);
            $DOB = date("Y-m-d", strtotime($dateofBirth));
            $reportPatientDetailArray = array_add($reportPatientDetailArray, 'date_of_birth', $DOB);
        endif;

        if (array_key_exists('ethnicity', $input) == true) :
            $reportPatientDetailArray = array_add($reportPatientDetailArray, 'ethnicity', $input['ethnicity']);
        endif;

        if (array_key_exists('sex', $input) == true) :
            $reportPatientDetailArray = array_add($reportPatientDetailArray, 'sex', $input['sex']);
        endif;

        if (array_key_exists('pathologyFile', $input) == true) :
            $fileUpload = $this->reportPatientDetail->uploadFile($input['pathologyFile'], $reportNumber);
            $reportPatientDetailArray = array_add($reportPatientDetailArray, 'pathology_file', $fileUpload);
        endif;

        // echo '<pre>';
        // print_r($reportPatientDetailArray);
        // echo '</pre>';

        // die();

        $reportReactionDetailArray = [
            'date_of_onset'             => $input['dateofOnset'], 
            'length_of_reaction'        => $input['lengthofReaction'], 
            // 'photo'                     => $input['reactionPhotoFile'], 
            // 'pathology_file'            => $input['reactionPathologyFile'], 
            'reaction_description'      => $input['reactionDescription'], 
            // 'specialised_diet'          => $input['diet'], 
            'life_threatening'          => $input['lifeThreatening'], 
            'medical_intervention'      => $input['medicalIntervention'], 
            'hospitalised'              => $input['hospitalised'], 
            'other'                     => $input['reactionOther'], 
            'full_recovery'             => $input['outcomeFullRecovery'], 
            'unknown'                   => $input['outcomeUnknown'], 
            'recovering'                => $input['outcomeRecovering']
        ];

        if (array_key_exists('reactionPhotoFile', $input) == true) :
            $fileUpload = $this->reportReactionDetail->uploadFile($input['reactionPhotoFile'], $reportNumber);
            $reportReactionDetailArray = array_add($reportReactionDetailArray, 'photo', $fileUpload);
        endif;

        if (array_key_exists('reactionPathologyFile', $input) == true) :
            $fileUpload = $this->reportReactionDetail->uploadFile($input['reactionPathologyFile'], $reportNumber);
            $reportReactionDetailArray = array_add($reportReactionDetailArray, 'pathology_file', $fileUpload);
        endif;

        if (array_key_exists('diet', $input) == true) :
            $reportReactionDetailArray = array_add($reportReactionDetailArray, 'specialised_diet', $input['diet']);
        endif;

        // echo '<pre>';
        // print_r($reportReactionDetailArray);
        // echo '</pre>';

        $reportOtherDetailArray = [
            // 'sequelae'              => $input['sequelae'], 
            // 'rechallenged'          => $input['rechallenged'], 
            'outcomes'              => $input['outcomes'], 
            'recovered'             => $input['recovered'], 
            // 'post_mortem_report'    => $input['postmortem'], 
            // 'death_known'           => $input['deathKnown'], 
            'cause_of_death'        => $input['causeofDeath']           
        ];

        if (array_key_exists('sequelae', $input) == true) :
            $reportOtherDetailArray = array_add($reportOtherDetailArray, 'sequelae', $input['sequelae']);
        endif;
        
        if (array_key_exists('rechallenged', $input) == true) :
            $reportOtherDetailArray = array_add($reportOtherDetailArray, 'rechallenged', $input['rechallenged']);
        endif;

        if (array_key_exists('postmortem', $input) == true) :
            $reportOtherDetailArray = array_add($reportOtherDetailArray, 'post_mortem_report', $input['postmortem']);
        endif;

        if (array_key_exists('deathKnown', $input) == true) :
            $reportOtherDetailArray = array_add($reportOtherDetailArray, 'death_known', $input['deathKnown']);
        endif;

        // echo '<pre>';
        // print_r($reportOtherDetailArray);
        // echo '</pre>';

        DB::beginTransaction();
        try {
            /* Save Report Array */
            $addedReport = $this->report->create($reportArray);

            /* Save Practitioner Detail Array */
            $reportPractitionerDetailArray = array_add($reportPractitionerDetailArray, 'report_id', $addedReport->id);
            $this->reportPractitionerDetail->create($reportPractitionerDetailArray);  
                    
            /* Save Primary Practitioner Detail Array */
            if ($input['isPrimaryPractitioner'] == 1) :
                $reportPrimaryPractitionerDetailArray = array_add($reportPrimaryPractitionerDetailArray, 'report_id', $addedReport->id);
                $this->reportPrimaryPractitionerDetail->create($reportPrimaryPractitionerDetailArray);
            endif;

            /* Save Patient Detail Array */
            $reportPatientDetailArray = array_add($reportPatientDetailArray, 'report_id', $addedReport->id);
            $this->reportPatientDetail->create($reportPatientDetailArray);

            /* Save Medicine Detail Array */
            $reportMedicineArray = [];

            foreach($input['formulation'] as $key => $value) {
                if ($value) :

                    $medicine = [
                        'report_id'     => $addedReport->id,    
                        'formulation'   => $input['formulation'][$key],
                        'dosage'        => $input['dosage'][$key],
                        'date_begun'    => $input['dateBegun'][$key],
                        'date_stopped'  => $input['dateStopped'][$key]
                    ];

                    array_push($reportMedicineArray, $medicine);

                endif;
            }
            // echo '<pre>';
            // print_r($reportMedicineArray);
            // echo '</pre>';
            $this->reportMedicineDetail->insert($reportMedicineArray);

            /* Save Reaction Detail Array */
            $reportReactionDetailArray = array_add($reportReactionDetailArray, 'report_id', $addedReport->id);
            $this->reportReactionDetail->create($reportReactionDetailArray);

            /* Save Other Detail Array */
            $reportOtherDetailArray = array_add($reportOtherDetailArray, 'report_id', $addedReport->id);
            $this->reportOtherDetail->create($reportOtherDetailArray);
              
            DB::commit();

            return redirect()->route('view-incident-report-form', ['reportId' => $addedReport->id]);

        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }   
        
    }
    
    public function updateIncidentReportForm(Request $request)
    {        
        $input = $request->all();

        // die();

        $userId = Auth::id();
        $reportId = $input['report_id'];
        $reportNumber = $input['report_number'];

        /* Report Array */
        $reportArray = [
            // 'report_number' => $reportNumber,
            'added_by'      => $input['addedBy'],
            // 'user_id'       => $userId
        ];

        // echo '<pre>';
        // print_r($reportArray);
        // echo '</pre>';

        /* Report Practitioner Detail Array */
        $reportPractitionerDetailArray = [
            'title'                     => $input['title'], 
            'first_name'                => $input['firstName'],
            'last_name'                 => $input['lastName'], 
            // 'medical_clinic'            => $input['medicalClinic'], 
            'medical_clinic_name'       => $input['medicalClinicName'], 
            // 'hospital'                  => $input['hospital'], 
            'hospital_name'             => $input['hospitalName'], 
            'hospital_unit_details'     => $input['hospitalUnitDetails'], 
            'email'                     => $input['email'], 
            'address_line_one'          => $input['address'], 
            'address_line_two'          => $input['address2'], 
            'suburb'                    => $input['suburb'],  
            'state'                     => $input['state'],
            'postcode'                  => $input['postcode'],
            'country'                   => $input['country'],
            'primary_practitioner'      => $input['isPrimaryPractitioner']
        ];

        if (array_key_exists('medicalClinic', $input) == true) :
            $reportPractitionerDetailArray = array_add($reportPractitionerDetailArray, 'medical_clinic', $input['medicalClinic']);
        endif;

        if (array_key_exists('hospital', $input) == true) :
            $reportPractitionerDetailArray = array_add($reportPractitionerDetailArray, 'hospital', $input['hospital']);
        endif;

        // echo '<pre>';
        // print_r($reportPractitionerDetailArray);
        // echo '</pre>';

        /* Report Primary Practitioner Detail Array */
        if ($input['isPrimaryPractitioner'] == 1) :
            $reportPrimaryPractitionerDetailArray = [
                'title'                     => $input['ppTitle'], 
                'first_name'                => $input['ppFirstName'],
                'last_name'                 => $input['ppLastName'], 
                // 'medical_clinic'            => $input['ppMedicalClinic'], 
                'medical_clinic_name'       => $input['ppMedicalClinicName'], 
                // 'hospital'                  => $input['ppHospital'], 
                'hospital_name'             => $input['ppHospitalName'], 
                'hospital_unit_details'     => $input['ppHospitalUnitDetails'], 
                'email'                     => $input['ppEmail'], 
                'address_line_one'          => $input['ppAddress'], 
                'address_line_two'          => $input['ppAddress2'], 
                'suburb'                    => $input['ppSuburb'],  
                'state'                     => $input['ppState'],
                'postcode'                  => $input['ppPostcode'],
                'country'                   => $input['ppCountry']
            ];

            if (array_key_exists('ppMedicalClinic', $input) == true) :
                $reportPrimaryPractitionerDetailArray = array_add($reportPrimaryPractitionerDetailArray, 'medical_clinic', $input['ppMedicalClinic']);
            endif;
    
            if (array_key_exists('ppHospital', $input) == true) :
                $reportPrimaryPractitionerDetailArray = array_add($reportPrimaryPractitionerDetailArray, 'hospital', $input['ppHospital']);
            endif;

            // echo '<pre>';
            // print_r($reportPrimaryPractitionerDetailArray);
            // echo '</pre>';
        endif; 

        $reportPatientDetailArray = [
            'patient_identification_number'         => $input['patientIdentificationNumber'], 
            'patient_initials'                      => $input['patientInitials'], 
            // 'ethnicity'                             => $input['ethnicity'], 
            // 'sex'                                   => $input['sex'], 
            // 'date_of_birth'                         => $DOB, 
            'weight'                                => $input['weight'], 
            'patient_symptoms'                      => $input['symptoms'], 
            'patient_diagnosis'                     => $input['diagnosis'], 
            'genetic_factors'                       => $input['genetic'], 
            'vitals_weight'                         => $input['patientWeight'], 
            'vitals_temp'                           => $input['patientTemp'], 
            'vitals_bp'                             => $input['patientBP'], 
            'vitals_hr'                             => $input['patientHR'], 
            'vitals_respiratory_rate'               => $input['patientRespiratoryRate'],       
            'vitals_oxygenation'                    => $input['patientOxygenation'], 
            'vitals_pain'                           => $input['patientPain'], 
            'vitals_appetite'                       => $input['patientAppetite'], 
            'vitals_urination'                      => $input['patientUrination'], 
            'vitals_defecation'                     => $input['patientDefecation'], 
            'vitals_vomiting'                       => $input['patientVomiting'], 
            'vitals_water_intake'                   => $input['patientWaterIntake'], 
            'vitals_food_intake'                    => $input['patientFoodIntake'], 
            'vitals_last_medication_time'           => $input['patientLastMedicationTime'], 
            'vitals_last_medication_dosage'         => $input['patientLastMedicationDosage'], 
            'vitals_last_medication_name'           => $input['patientLastMedicationName'], 
            'pathology'                             => $input['pathology']
        ];

        if (isset($input['dateOfBirth'])) :
            $dateofBirth = str_replace('/', '-', $input['dateOfBirth']);
            $DOB = date("Y-m-d", strtotime($dateofBirth));
            $reportPatientDetailArray = array_add($reportPatientDetailArray, 'date_of_birth', $DOB);
        endif;

        if (array_key_exists('ethnicity', $input) == true) :
            $reportPatientDetailArray = array_add($reportPatientDetailArray, 'ethnicity', $input['ethnicity']);
        endif;

        if (array_key_exists('sex', $input) == true) :
            $reportPatientDetailArray = array_add($reportPatientDetailArray, 'sex', $input['sex']);
        endif;

        if (array_key_exists('pathologyFile', $input) == true) :
            $fileUpload = $this->reportPatientDetail->uploadFile($input['pathologyFile'], $reportNumber);
            $reportPatientDetailArray = array_add($reportPatientDetailArray, 'pathology_file', $fileUpload);
        endif;

        // echo '<pre>';
        // print_r($reportPatientDetailArray);
        // echo '</pre>';

        // die();

        $reportReactionDetailArray = [
            'date_of_onset'             => $input['dateofOnset'], 
            'length_of_reaction'        => $input['lengthofReaction'], 
            // 'photo'                     => $input['reactionPhotoFile'], 
            // 'pathology_file'            => $input['reactionPathologyFile'], 
            'reaction_description'      => $input['reactionDescription'], 
            // 'specialised_diet'          => $input['diet'], 
            'life_threatening'          => $input['lifeThreatening'], 
            'medical_intervention'      => $input['medicalIntervention'], 
            'hospitalised'              => $input['hospitalised'], 
            'other'                     => $input['reactionOther'], 
            'full_recovery'             => $input['outcomeFullRecovery'], 
            'unknown'                   => $input['outcomeUnknown'], 
            'recovering'                => $input['outcomeRecovering']
        ];

        if (array_key_exists('reactionPhotoFile', $input) == true) :
            $fileUpload = $this->reportReactionDetail->uploadFile($input['reactionPhotoFile'], $reportNumber);
            $reportReactionDetailArray = array_add($reportReactionDetailArray, 'photo', $fileUpload);
        endif;

        if (array_key_exists('reactionPathologyFile', $input) == true) :
            $fileUpload = $this->reportReactionDetail->uploadFile($input['reactionPathologyFile'], $reportNumber);
            $reportReactionDetailArray = array_add($reportReactionDetailArray, 'pathology_file', $fileUpload);
        endif;

        if (array_key_exists('diet', $input) == true) :
            $reportReactionDetailArray = array_add($reportReactionDetailArray, 'specialised_diet', $input['diet']);
        endif;

        // echo '<pre>';
        // print_r($reportReactionDetailArray);
        // echo '</pre>';

        $reportOtherDetailArray = [
            // 'sequelae'              => $input['sequelae'], 
            // 'rechallenged'          => $input['rechallenged'], 
            'outcomes'              => $input['outcomes'], 
            'recovered'             => $input['recovered'], 
            // 'post_mortem_report'    => $input['postmortem'], 
            // 'death_known'           => $input['deathKnown'], 
            'cause_of_death'        => $input['causeofDeath']           
        ];

        if (array_key_exists('sequelae', $input) == true) :
            $reportOtherDetailArray = array_add($reportOtherDetailArray, 'sequelae', $input['sequelae']);
        endif;
        
        if (array_key_exists('rechallenged', $input) == true) :
            $reportOtherDetailArray = array_add($reportOtherDetailArray, 'rechallenged', $input['rechallenged']);
        endif;

        if (array_key_exists('postmortem', $input) == true) :
            $reportOtherDetailArray = array_add($reportOtherDetailArray, 'post_mortem_report', $input['postmortem']);
        endif;

        if (array_key_exists('deathKnown', $input) == true) :
            $reportOtherDetailArray = array_add($reportOtherDetailArray, 'death_known', $input['deathKnown']);
        endif;

        // echo '<pre>';
        // print_r($reportOtherDetailArray);
        // echo '</pre>';

        DB::beginTransaction();
        try {
            /* Update Report Array */
            $this->report->where('id', '=', $reportId)->update($reportArray);

            /* Update Practitioner Detail Array */
            $this->reportPractitionerDetail->where('report_id', '=', $reportId)->update($reportPractitionerDetailArray);  
                    
            /* Update Primary Practitioner Detail Array */
            if ($input['isPrimaryPractitioner'] == 1) :
                $this->reportPrimaryPractitionerDetail->where('report_id', '=', $reportId)->delete();   

                $reportPrimaryPractitionerDetailArray = array_add($reportPrimaryPractitionerDetailArray, 'report_id', $reportId);
                $this->reportPrimaryPractitionerDetail->create($reportPrimaryPractitionerDetailArray);  
            endif;

            /* Update Patient Detail Array */
            $this->reportPatientDetail->where('report_id', '=', $reportId)->update($reportPatientDetailArray);

            /* Update Medicine Array */
            $this->reportMedicineDetail->where('report_id', '=', $reportId)->delete();
            $reportMedicineArray = [];

            foreach($input['formulation'] as $key => $value) {
                if ($value) :

                    $medicine = [
                        'report_id'     => $reportId,    
                        'formulation'   => $input['formulation'][$key],
                        'dosage'        => $input['dosage'][$key],
                        'date_begun'    => $input['dateBegun'][$key],
                        'date_stopped'  => $input['dateStopped'][$key]
                    ];

                    array_push($reportMedicineArray, $medicine);

                endif;
            }
            // echo '<pre>';
            // print_r($reportMedicineArray);
            // echo '</pre>';
            $this->reportMedicineDetail->insert($reportMedicineArray);

            /* Update Reaction Detail Array */
            $this->reportReactionDetail->where('report_id', '=', $reportId)->update($reportReactionDetailArray);

            /* Update Other Detail Array */
            $this->reportOtherDetail->where('report_id', '=', $reportId)->update($reportOtherDetailArray);
              
            DB::commit();

            return redirect()->route('view-incident-report-form', ['reportId' => $reportId]);

        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }           
    }

    public function viewReport($reportId)
    {
        $report =  $this->report
            ->with(
                'reportPractitionerDetail',
                'reportPrimaryPractitionerDetail',
                'reportPatientDetail',
                'reportMedicineDetail',
                'reportReactionDetail',
                'reportOtherDetail'
            )
            ->where('id', '=', $reportId)
            ->first();
        
        // echo '<pre>';
        // print_r($report);
        // echo '<pre>';

        // die();

        return view('incident_report_view', [
            'report' => $report
        ]);
    }

    public function editReport($reportId)
    {
        $report =  $this->report
            ->with(
                'reportPractitionerDetail',
                'reportPrimaryPractitionerDetail',
                'reportPatientDetail',
                'reportMedicineDetail',
                'reportReactionDetail',
                'reportOtherDetail'
            )
            ->where('id', '=', $reportId)
            ->first();
        
        // echo '<pre>';
        // print_r($report);
        // echo '<pre>';

        // die();

        return view('incident_report_edit', [
            'report' => $report
        ]);
    }

    public function exportFile($reportId) {

        $report =  $this->report
            ->with(
                'reportPractitionerDetail',
                'reportPrimaryPractitionerDetail',
                'reportPatientDetail',
                'reportMedicineDetail',
                'reportReactionDetail',
                'reportOtherDetail'
            )
            ->where('id', '=', $reportId)
            ->first()->toArray();
        
        $csvData = array_flatten($report);
        
        // echo "<pre>";
        // print_r($csvData);
        // echo "</pre>";
        // die();

        return \Excel::create($report['report_number'].'_report_data', function($excel) use ($csvData) {

            $excel->sheet('Report Data', function($sheet) use ($csvData)
            {
                $sheet->fromArray($csvData);
            });

        })->download('csv');

    } 

    function array_flatten($array) {
        if (!is_array($array)) {
          return FALSE;
        }
        $result = array();
        foreach ($array as $key => $value) {
          if (is_array($value)) {
            $result = array_merge($result, array_flatten($value));
          }
          else {
            $result[$key] = $value;
          }
        }
        return $result;
      } 

}
