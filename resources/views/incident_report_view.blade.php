@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">Echelon Incident Report</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <h5>Report Number: <span class="badge badge-secondary">{{ $report->report_number }}</span></h5>
                    </div>

                    <div class="form-group">
                        Author’s Name: {{ $report->added_by }}
                    </div>
                    <hr class="mb-4">

                    <div class="form-group">
                        <h5>Practitioner Details:</h5>
                    </div>
                    <div class="form-group">
                        Name: {{ $report->reportPractitionerDetail->title }} {{ $report->reportPractitionerDetail->first_name }} {{ $report->reportPractitionerDetail->last_name }}
                    </div>
                    <div class="form-group">   
                        Medical Clinic:
                        @if ($report->reportPractitionerDetail->medical_clinic === 1)
                            Yes - {{ $report->reportPractitionerDetail->medical_clinic_name }}
                        @elseif ($report->reportPractitionerDetail->medical_clinic === 0)
                            No
                        @endif
                    </div>
                    <div class="form-group">   
                        Hospital:
                        @if ($report->reportPractitionerDetail->hospital === 1)
                            Yes - {{ $report->reportPractitionerDetail->hospital_name }}
                            <p>Hospital Unit Details: {{ $report->reportPractitionerDetail->hospital_unit_details }}</p>
                        @elseif ($report->reportPractitionerDetail->hospital === 0)
                            No
                        @endif
                    </div>
                    <div class="form-group"> 
                        Email: {{ $report->reportPractitionerDetail->email }}
                    </div> 
                    <div class="form-group"> 
                        Address: {{ $report->reportPractitionerDetail->address_line_one }}
                        @if ($report->reportPractitionerDetail->address_line_two)
                            ,{{ $report->reportPractitionerDetail->address_line_two }}    
                        @endif
                    </div> 
                    <div class="form-group"> 
                        Suburb: {{ $report->reportPractitionerDetail->suburb }}
                    </div> 
                    <div class="form-group"> 
                        State: {{ $report->reportPractitionerDetail->state }}
                    </div> 
                    <div class="form-group"> 
                        Postcode: {{ $report->reportPractitionerDetail->postcode }}
                    </div> 
                    <div class="form-group"> 
                        Country: {{ $report->reportPractitionerDetail->country }}
                    </div> 
                    <div class="form-group"> 
                        Primary Practitioner Known:
                        @if ($report->reportPractitionerDetail->primary_practitioner === 1)
                            Yes
                        @else
                            No
                        @endif
                    </div>   

                    @if ($report->reportPractitionerDetail->primary_practitioner === 1)
                        <hr class="mb-4">
                        <div class="form-group">
                            <h5>Primary Practitioner Details:</h5>
                        </div>
                        <div class="form-group">
                            Name: {{ $report->reportPrimaryPractitionerDetail->title }} {{ $report->reportPrimaryPractitionerDetail->first_name }} {{ $report->reportPrimaryPractitionerDetail->last_name }}
                        </div>
                        <div class="form-group">   
                            Medical Clinic:
                            @if ($report->reportPrimaryPractitionerDetail->medical_clinic === 1)
                                Yes - {{ $report->reportPrimaryPractitionerDetail->medical_clinic_name }}
                            @elseif ($report->reportPrimaryPractitionerDetail->medical_clinic === 0)
                                No
                            @endif
                        </div>
                        <div class="form-group">   
                            Hospital:
                            @if ($report->reportPrimaryPractitionerDetail->hospital === 1)
                                Yes - {{ $report->reportPrimaryPractitionerDetail->hospital_name }}
                                <p>Hospital Unit Details: {{ $report->reportPrimaryPractitionerDetail->hospital_unit_details }}</p>
                            @elseif ($report->reportPrimaryPractitionerDetail->hospital === 0)
                                No
                            @endif
                        </div>
                        <div class="form-group"> 
                            Email: {{ $report->reportPrimaryPractitionerDetail->email }}
                        </div> 
                        <div class="form-group"> 
                            Address: {{ $report->reportPrimaryPractitionerDetail->address_line_one }}
                            @if ($report->reportPrimaryPractitionerDetail->address_line_two)
                                ,{{ $report->reportPrimaryPractitionerDetail->address_line_two }}    
                            @endif
                        </div> 
                        <div class="form-group"> 
                            Suburb: {{ $report->reportPrimaryPractitionerDetail->suburb }}
                        </div> 
                        <div class="form-group"> 
                            State: {{ $report->reportPrimaryPractitionerDetail->state }}
                        </div> 
                        <div class="form-group"> 
                            Postcode: {{ $report->reportPrimaryPractitionerDetail->postcode }}
                        </div> 
                        <div class="form-group"> 
                            Country: {{ $report->reportPrimaryPractitionerDetail->country }}
                        </div> 
                    @endif

                    <hr class="mb-4">
                    <div class="form-group">
                        <h5>Patient Details:</h5>
                    </div>                    
                    <div class="form-group"> 
                        Patient Identification Number: {{ $report->reportPatientDetail->patient_identification_number }}
                    </div> 
                    <div class="form-group"> 
                        Patient Initials: {{ $report->reportPatientDetail->patient_initials }}
                    </div>  
                    <div class="form-group"> 
                        Ethnicity: {{ $report->reportPatientDetail->ethnicity }}
                    </div> 
                    <div class="form-group"> 
                        Sex: {{ $report->reportPatientDetail->sex }}
                    </div> 
                    <div class="form-group"> 
                        Date of Birth: {{ $report->reportPatientDetail->date_of_birth }}
                    </div> 
                    <div class="form-group"> 
                        Weight (Kg): {{ $report->reportPatientDetail->weight }}
                    </div> 
                    <div class="form-group"> 
                        Patient symptoms prior prescription: <p>{{ $report->reportPatientDetail->patient_symptoms }}</p>
                    </div>
                    <div class="form-group">                         
                        Patient diagnosis resulting in prescription: <p>{{ $report->reportPatientDetail->patient_diagnosis }}</p>
                    </div>
                    <div class="form-group">                         
                        Hereditary or genetic factors noteworthy for this report: <p>{{ $report->reportPatientDetail->genetic_factors }}</p>
                    </div>
                    <hr class="mb-4">

                    <div class="form-group">
                        <h5>Vitals of Patient:</h5>
                    </div>                    
                    <div class="row">                         
                        <div class="col-md-3 mb-3">
                            Weight: {{ $report->reportPatientDetail->vitals_weight }}
                        </div>                                 
                        <div class="col-md-3 mb-3">
                            Temperature: {{ $report->reportPatientDetail->vitals_temp }}
                        </div>                      
                        <div class="col-md-3 mb-3">     
                            Blood Pressure: {{ $report->reportPatientDetail->vitals_bp }}
                        </div>                     
                        <div class="col-md-3 mb-3">     
                            Hear Rate: {{ $report->reportPatientDetail->vitals_hr }}
                        </div> 
                    </div>
                    <div class="row">                         
                        <div class="col-md-3 mb-3">
                            Respiratory Rate: {{ $report->reportPatientDetail->vitals_respiratory_rate }}
                        </div>                                 
                        <div class="col-md-3 mb-3">
                            Oxygenation: {{ $report->reportPatientDetail->vitals_oxygenation }}
                        </div>                      
                        <div class="col-md-3 mb-3">     
                            Pain: {{ $report->reportPatientDetail->vitals_pain }}
                        </div>                     
                        <div class="col-md-3 mb-3">     
                            Appetite: {{ $report->reportPatientDetail->vitals_appetite }}
                        </div> 
                    </div>
                    <div class="row">                         
                        <div class="col-md-3 mb-3">
                            Urination: {{ $report->reportPatientDetail->vitals_urination }}
                        </div>                                 
                        <div class="col-md-3 mb-3">
                            Defecation: {{ $report->reportPatientDetail->vitals_defecation }}
                        </div>                      
                        <div class="col-md-3 mb-3">     
                            Vomiting: {{ $report->reportPatientDetail->vitals_vomiting }}
                        </div>                     
                        <div class="col-md-3 mb-3">     
                            Water Intake (mL): {{ $report->reportPatientDetail->vitals_water_intake }}
                        </div> 
                    </div>
                    <div class="row">                         
                        <div class="col-md-3 mb-3">
                            Food Intake: {{ $report->reportPatientDetail->vitals_food_intake }}
                        </div>                                 
                        <div class="col-md-3 mb-3">
                            Last Medication Time: {{ $report->reportPatientDetail->vitals_last_medication_time }}
                        </div>                      
                        <div class="col-md-3 mb-3">     
                            Last Medication Dosage: {{ $report->reportPatientDetail->vitals_last_medication_dosage }}
                        </div>                     
                        <div class="col-md-3 mb-3">     
                            Last Medication Name: {{ $report->reportPatientDetail->vitals_last_medication_name }}
                        </div> 
                    </div>
                    <div class="form-group">
                        Pathology to support diagnosis: <p>{{ $report->reportPatientDetail->pathology }}</p>
                        @if ($report->reportPatientDetail->pathology_file)
                            Pathology Report: <a href="{{ url('/uploads/reports/'.$report->reportPatientDetail->pathology_file) }}" target="_blank">View</a>
                        @else
                            Pathology Report: None
                        @endif
                    </div>

                    <hr class="mb-4">

                    <div class="form-group">
                        <h5>Suspected medicine(s):</h5>
                    </div>  

                    @forelse ($report->reportMedicineDetail as $medicine)
                        <div class="row">   
                            <div class="col-md-3 mb-3">
                            Formulation in question: {{ $medicine->formulation }}
                            </div> 
                            <div class="col-md-3 mb-3">
                            Dosage: {{ $medicine->dosage }}
                            </div> 
                            <div class="col-md-3 mb-3">
                            Date Begun: {{ $medicine->date_begun }}
                            </div> 
                            <div class="col-md-3 mb-3">
                            Date Stopped:   {{ $medicine->date_stopped }}
                            </div> 
                        </div>
                    @empty
                        <p>None</p>
                    @endforelse

                    <hr class="mb-4">

                    <div class="form-group">
                        <h5>Other medicine(s) taken at the time of the reaction:</h5>
                        <h5>Reaction(s):</h5>
                    </div>  
                    <div class="row"> 
                        <div class="col-md-6 mb-3">
                            Date of Onset: {{ $report->reportReactionDetail->date_of_onset }}
                        </div>
                        <div class="col-md-6 mb-3">
                            Length of Reaction: {{ $report->reportReactionDetail->length_of_reaction }} 
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-md-6 mb-3">
                            @if ($report->reportReactionDetail->photo)
                                Photo: <a href="{{ url('/uploads/reports/'.$report->reportReactionDetail->photo) }}" target="_blank">View</a>
                            @else
                                Photo: None
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">                            
                            @if ($report->reportReactionDetail->pathology_file)
                                Pathology: <a href="{{ url('/uploads/reports/'.$report->reportReactionDetail->pathology_file) }}" target="_blank">View</a>
                            @else
                                Pathology: None
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        Describe reaction in as much details as possible: <p>{{ $report->reportReactionDetail->reaction_description }}</p>
                    </div>
                    <div class="form-group">   
                        Has patient kept a current diet diary or has undertaken a specialised diet:
                        @if ($report->reportReactionDetail->specialised_diet === 1)
                            Yes
                        @elseif ($report->reportReactionDetail->specialised_diet === 0)
                            No
                        @endif
                    </div>

                    <div class="form-group">
                        <h5>Seriousness of Reaction:</h5>                        
                    </div> 
                    <div class="row">                         
                        <div class="col-md-4 mb-3">
                            Life Threatening: {{ $report->reportReactionDetail->life_threatening }}
                        </div>                                 
                        <div class="col-md-4 mb-3">
                            Medical Intervention: {{ $report->reportReactionDetail->medical_intervention }}
                        </div>                      
                        <div class="col-md-4 mb-3">     
                            Hospitalised: {{ $report->reportReactionDetail->hospitalised }}
                        </div>    
                    </div>
                    <div class="form-group">
                        Other: <p>{{ $report->reportReactionDetail->other }}</p>
                    </div>

                    
                    <div class="form-group">
                        <h5>Seriousness of Reaction:</h5>                        
                    </div> 
                    <div class="row">                         
                        <div class="col-md-4 mb-3">
                            Full Recovery: {{ $report->reportReactionDetail->full_recovery }}
                        </div>                                 
                        <div class="col-md-4 mb-3">
                            Unknown: {{ $report->reportReactionDetail->unknown }}
                        </div>                      
                        <div class="col-md-4 mb-3">     
                            Hospitalised: {{ $report->reportReactionDetail->recovering }}
                        </div>    
                    </div>

                    <hr class="mb-4">

                    <div class="form-group">   
                        Sequelae:
                        @if ($report->reportOtherDetail->sequelae === 1)
                            Yes
                        @elseif ($report->reportOtherDetail->sequelae === 0)
                            No
                        @endif
                    </div>
                    <div class="form-group">   
                        Was medication and dosage rechallenged:
                        @if ($report->reportOtherDetail->rechallenged === 1)
                            Yes
                        @elseif ($report->reportOtherDetail->rechallenged === 0)
                            No
                        @endif
                    </div>                    
                    <div class="form-group">
                        Outcomes? <p>{{ $report->reportOtherDetail->outcomes }}</p>
                    </div>
                    <div class="form-group">
                        Reaction recovered? {{ $report->reportOtherDetail->recovered }}
                    </div>
                    <div class="form-group">   
                        In the event of death, is there a post mortem report:
                        @if ($report->reportOtherDetail->post_mortem_report === 1)
                            Yes
                        @elseif ($report->reportOtherDetail->post_mortem_report === 0)
                            No
                        @endif
                    </div> 
                    <div class="form-group">  
                        Is the cause of death known:
                        @if ($report->reportOtherDetail->death_known === 1)
                            Yes
                        @elseif ($report->reportOtherDetail->death_known === 0)
                            No
                        @endif
                    </div> 
                    <div class="form-group">
                        Cause of Death {{ $report->reportOtherDetail->cause_of_death }}
                    </div>

                    <hr class="mb-4">

                    @if (Auth::user()->isAdmin())
                        <a href="{{ route('incident-report-export', [ 'reportId' => $report->id ]) }}" class="btn btn-info btn-block">Export to CSV</a>  
                    @else                        
                        <a href="{{ route('edit-incident-report-form', [ 'reportId' => $report->id ]) }}" class="btn btn-info btn-block">Edit Report</a> 
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection