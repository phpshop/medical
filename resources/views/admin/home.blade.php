@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <div class="row">                        
                        <div class="col-md-12 mb-3">
                            <div class="table-responsive">
                                <table class="table table-bordered text-center">
                                    <caption>List of reports</caption>
                                    <thead>
                                        <tr>
                                        <th scope="col">Report #</th>
                                        <th scope="col">Author</th>
                                        <th scope="col">Created</th>
                                        <th scope="col">Last Updated</th>
                                        <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($reports as $report)
                                            <tr>
                                                <th scope="row">{{ $report->report_number }}</th>
                                                <td>{{ $report->added_by }}</td>
                                                <td>{{ $report->created_at }}</td>
                                                <td>{{ $report->updated_at }}</td>
                                                <td>
                                                    <a href="{{ route('view-incident-report-form', [ 'reportId' => $report->id ]) }}" class="btn btn-info btn-sm" role="button">View</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
