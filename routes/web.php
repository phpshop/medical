<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('login', 'LoginController@showLoginForm');

// Route::get('login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);

// Route::post('login', 'LoginController@do');

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/admin/dashboard', function () {
        return view('admin.home');
    });
    Route::get('/export/{reportId}', 'IncidentController@exportFile')->name('incident-report-export');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/report/create', 'IncidentController@index')->name('incident-report-form');
    Route::post('/incident-form-post', 'IncidentController@postIncidentReportForm')->name('incident-form-post');
    Route::post('/incident-form-update', 'IncidentController@updateIncidentReportForm')->name('incident-form-update');

    Route::get('/report/{reportId}', 'IncidentController@viewReport')->name('view-incident-report-form');
    Route::get('/report/edit/{reportId}', 'IncidentController@editReport')->name('edit-incident-report-form');
});

