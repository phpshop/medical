<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ReportMedicineDetail extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'report_medicine_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'report_id',
        'formulation', 
        'dosage', 
        'date_begun', 
        'date_stopped'
    ];

}
