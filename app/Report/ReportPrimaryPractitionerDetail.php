<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ReportPrimaryPractitionerDetail extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'report_primary_practitioner_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'report_id', 
        'title', 
        'first_name', 
        'last_name', 
        'medical_clinic', 
        'medical_clinic_name', 
        'hospital', 
        'hospital_name', 
        'hospital_unit_details', 
        'email', 
        'address_line_one', 
        'address_line_two', 
        'suburb', 
        'state', 
        'postcode', 
        'country', 
        'created_at', 
        'updated_at'
    ];

}
