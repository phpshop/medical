@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">Echelon Incident Report</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!-- You are logged in! -->

                    <form action="{{URL::to('/incident-form-update')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <h5>Incident Report Number: <span class="badge badge-secondary">{{ $report->report_number }}</span></h5>
                        </div>
                        <div class="form-group">
                            Author’s Name: {{ Auth::user()->name }}
                            <input type="hidden" name="report_id" value="{{ $report->id }}">
                            <input type="hidden" name="report_number" value="{{ $report->report_number }}">
                        </div>

                        <h6>Practitioner Details:</h6>

                        <div class="row">
                            <div class="col-md-2 mb-3">
                                <label for="title">Title</label>
                                <select class="form-control" id="title" name="title" >
                                    <option value="">Choose...</option>
                                    <option @if($report->reportPractitionerDetail->title === 'Mr.') {{ 'selected' }} @endif>Mr.</option>
                                    <option @if($report->reportPractitionerDetail->title === 'Mrs.') {{ 'selected' }} @endif>Mrs.</option>
                                    <option @if($report->reportPractitionerDetail->title === 'Miss') {{ 'selected' }} @endif>Miss</option>
                                    <option @if($report->reportPractitionerDetail->title === 'Ms.') {{ 'selected' }} @endif>Ms.</option>
                                    <option @if($report->reportPractitionerDetail->title === 'Dr.') {{ 'selected' }} @endif>Dr.</option>
                                    <option @if($report->reportPractitionerDetail->title === 'Prof.') {{ 'selected' }} @endif>Prof.</option>
                                    <option @if($report->reportPractitionerDetail->title === 'Rev.') {{ 'selected' }} @endif>Rev.</option>
                                    <option @if($report->reportPractitionerDetail->title === 'Other') {{ 'selected' }} @endif>Other</option>
                                </select>
                                <div class="invalid-feedback">
                                Please select a valid tittle.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="firstName">First name</label>
                                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" value="{{ $report->reportPractitionerDetail->first_name }}" >
                                <div class="invalid-feedback">
                                Valid first name is required.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="lastName">Last name</label>
                                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" value="{{ $report->reportPractitionerDetail->last_name }}" >
                                <div class="invalid-feedback">
                                Valid last name is required.
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="medicalClinic">Medical Clinic: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="medicalClinic" id="medicalClinicYes" value="1" @if($report->reportPractitionerDetail->medical_clinic === 1) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="medicalClinicYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="medicalClinic" id="medicalClinicNo" value="0" @if($report->reportPractitionerDetail->medical_clinic === 0) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="medicalClinicNo">No</label>
                                </div>
                                <input type="text" class="form-control" id="medicalClinicName" name="medicalClinicName" placeholder="Medical Clinic Name" value="{{ $report->reportPractitionerDetail->medical_clinic_name }}" >
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="hospital">Hospital: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="hospital" id="hospitalYes" value="1" @if($report->reportPractitionerDetail->hospital === 1) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="hospitalYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="hospital" id="hospitalNo" value="0" @if($report->reportPractitionerDetail->hospital === 0) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="hospitalNo">No</label>
                                </div>
                                <input type="text" class="form-control" id="hospitalName" name="hospitalName" placeholder="Hospital Name" value="{{ $report->reportPractitionerDetail->hospital_name }}" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="hospitalUnitDetails">If Hospital, Unit Details: </label>
                                <textarea class="form-control" id="hospitalUnitDetails" name="hospitalUnitDetails" placeholder="Hospital Unit Details" >{{ $report->reportPractitionerDetail->hospital_unit_details }}</textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com" value="{{ $report->reportPractitionerDetail->email }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St" value="{{ $report->reportPractitionerDetail->address_line_one }}">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
                                <input type="text" class="form-control" id="address2" name="address2" placeholder="Apartment or suite" value="{{ $report->reportPractitionerDetail->address_line_two }}">            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="suburb">Suburb</label>
                                <input type="text" class="form-control" id="suburb" name="suburb" placeholder="Suburb" value="{{ $report->reportPractitionerDetail->suburb }}" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="state">State</label>
                                <input type="text" class="form-control" id="state" name="state" placeholder="State" value="{{ $report->reportPractitionerDetail->state }}" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="postcode">Postcode</label>
                                <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode" value="{{ $report->reportPractitionerDetail->postcode }}" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="country">Country</label>
                                <input type="text" class="form-control" id="country" name="country" placeholder="Country" value="{{ $report->reportPractitionerDetail->country }}">
                            </div>
                        </div>

                        <hr class="mb-4">
                        
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="isPrimaryPractitioner">Is Primary Practitioner Known: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="isPrimaryPractitioner" id="primaryPractitionerYes" value="1" required @if($report->reportPractitionerDetail->primary_practitioner === 1) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="primaryPractitionerYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="isPrimaryPractitioner" id="primaryPractitionerNo" value="0" @if($report->reportPractitionerDetail->primary_practitioner === 0) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="primaryPractitionerNo">No</label>
                                </div>
                            </div>
                        </div>

                        <h6>If Yes, then Primary Practitioner Details:</h6>   

                        <div class="row">
                            <div class="col-md-5 mb-3">
                                <label for="">If same as above select here </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input position-static" type="checkbox" id="sameAsPractitioner" value="Yes" aria-label="...">
                                </div>
                            </div>
                        </div>

                        @if($report->reportPrimaryPractitionerDetail)

                        <div class="row">
                            <div class="col-md-2 mb-3">
                                <label for="title">Title</label>
                                <select class="form-control" id="ppTitle" name="ppTitle" >
                                    <option value="">Choose...</option>
                                    <option @if($report->reportPrimaryPractitionerDetail->title === 'Mr.') {{ 'selected' }} @endif>Mr.</option>
                                    <option @if($report->reportPrimaryPractitionerDetail->title === 'Mrs.') {{ 'selected' }} @endif>Mrs.</option>
                                    <option @if($report->reportPrimaryPractitionerDetail->title === 'Miss') {{ 'selected' }} @endif>Miss</option>
                                    <option @if($report->reportPrimaryPractitionerDetail->title === 'Ms.') {{ 'selected' }} @endif>Ms.</option>
                                    <option @if($report->reportPrimaryPractitionerDetail->title === 'Dr.') {{ 'selected' }} @endif>Dr.</option>
                                    <option @if($report->reportPrimaryPractitionerDetail->title === 'Prof.') {{ 'selected' }} @endif>Prof.</option>
                                    <option @if($report->reportPrimaryPractitionerDetail->title === 'Rev.') {{ 'selected' }} @endif>Rev.</option>
                                    <option @if($report->reportPrimaryPractitionerDetail->title === 'Other') {{ 'selected' }} @endif>Other</option>
                                </select>
                                <div class="invalid-feedback">
                                Please select a valid tittle.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="firstName">First name</label>
                                <input type="text" class="form-control" id="ppFirstName" name="ppFirstName" placeholder="First Name" value="{{ $report->reportPrimaryPractitionerDetail->first_name }}" >
                                <div class="invalid-feedback">
                                Valid first name is required.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="lastName">Last name</label>
                                <input type="text" class="form-control" id="ppLastName" name="ppLastName" placeholder="Last Name" value="{{ $report->reportPrimaryPractitionerDetail->last_name }}" >
                                <div class="invalid-feedback">
                                Valid last name is required.
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="ppMedicalClinic">Medical Clinic: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppMedicalClinic" id="ppMedicalClinicYes" value="1" @if($report->reportPrimaryPractitionerDetail->medical_clinic === 1) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ppMedicalClinicYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppMedicalClinic" id="ppMedicalClinicNo" value="0" @if($report->reportPrimaryPractitionerDetail->medical_clinic === 0) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ppMedicalClinicNo">No</label>
                                </div>
                                <input type="text" class="form-control" id="ppMedicalClinicName" name="ppMedicalClinicName" placeholder="Medical Clinic Name" value="{{ $report->reportPrimaryPractitionerDetail->medical_clinic_name }}">
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="ppHospital">Hospital: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppHospital" id="ppHospitalYes" value="1" @if($report->reportPrimaryPractitionerDetail->hospital === 1) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ppHospitalYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppHospital" id="ppHospitalNo" value="0" @if($report->reportPrimaryPractitionerDetail->hospital === 0) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ppHospitalNo">No</label>
                                </div>
                                <input type="text" class="form-control" id="ppHospitalName" name="ppHospitalName" placeholder="Hospital Name" value="" value="{{ $report->reportPrimaryPractitionerDetail->hospital_name }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppHospitalUnitDetails">If Hospital, Unit Details: </label>
                                <textarea class="form-control" id="ppHospitalUnitDetails" name="ppHospitalUnitDetails" placeholder="Hospital Unit Details" >{{ $report->reportPrimaryPractitionerDetail->hospital_unit_details }}</textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppEmail">Email</label>
                                <input type="email" class="form-control" id="ppEmail" name="ppEmail" placeholder="you@example.com" value="{{ $report->reportPrimaryPractitionerDetail->email }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppAddress">Address</label>
                                <input type="text" class="form-control" id="ppAddress" name="ppAddress" placeholder="1234 Main St" value="{{ $report->reportPrimaryPractitionerDetail->address_line_one }}" >
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppAddress2">Address 2 <span class="text-muted">(Optional)</span></label>
                                <input type="text" class="form-control" id="ppAddress2" name="ppAddress2" placeholder="Apartment or suite" value="{{ $report->reportPrimaryPractitionerDetail->address_line_two }}">            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="ppSuburb">Suburb</label>
                                <input type="text" class="form-control" id="ppSuburb" name="ppSuburb" placeholder="Suburb" value="{{ $report->reportPrimaryPractitionerDetail->suburb }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="ppState">State</label>
                                <input type="text" class="form-control" id="ppState" name="ppState" placeholder="State" value="{{ $report->reportPrimaryPractitionerDetail->state }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="ppPostcode">Postcode</label>
                                <input type="text" class="form-control" id="ppPostcode" name="ppPostcode" placeholder="Postcode" value="{{ $report->reportPrimaryPractitionerDetail->postcode }}" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="ppCountry">Country</label>
                                <input type="text" class="form-control" id="ppCountry" name="ppCountry" placeholder="Country" value="{{ $report->reportPrimaryPractitionerDetail->country }}">
                            </div>
                        </div>

                        @else

                        <div class="row">
                            <div class="col-md-2 mb-3">
                                <label for="title">Title</label>
                                <select class="form-control" id="ppTitle" name="ppTitle" >
                                    <option value="">Choose...</option>
                                    <option>Mr.</option>
                                    <option>Mrs.</option>
                                    <option>Miss</option>
                                    <option>Ms.</option>
                                    <option>Dr.</option>
                                    <option>Prof.</option>
                                    <option>Rev.</option>
                                    <option>Other</option>
                                </select>
                                <div class="invalid-feedback">
                                Please select a valid tittle.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="firstName">First name</label>
                                <input type="text" class="form-control" id="ppFirstName" name="ppFirstName" placeholder="First Name" value="" >
                                <div class="invalid-feedback">
                                Valid first name is required.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="lastName">Last name</label>
                                <input type="text" class="form-control" id="ppLastName" name="ppLastName" placeholder="Last Name" value="" >
                                <div class="invalid-feedback">
                                Valid last name is required.
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="ppMedicalClinic">Medical Clinic: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppMedicalClinic" id="ppMedicalClinicYes" value="1">
                                    <label class="form-check-label" for="ppMedicalClinicYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppMedicalClinic" id="ppMedicalClinicNo" value="0">
                                    <label class="form-check-label" for="ppMedicalClinicNo">No</label>
                                </div>
                                <input type="text" class="form-control" id="ppMedicalClinicName" name="ppMedicalClinicName" placeholder="Medical Clinic Name" value="" >
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="ppHospital">Hospital: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppHospital" id="ppHospitalYes" value="1">
                                    <label class="form-check-label" for="ppHospitalYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppHospital" id="ppHospitalNo" value="0">
                                    <label class="form-check-label" for="ppHospitalNo">No</label>
                                </div>
                                <input type="text" class="form-control" id="ppHospitalName" name="ppHospitalName" placeholder="Hospital Name" value="" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppHospitalUnitDetails">If Hospital, Unit Details: </label>
                                <textarea class="form-control" id="ppHospitalUnitDetails" name="ppHospitalUnitDetails" placeholder="Hospital Unit Details" ></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppEmail">Email</label>
                                <input type="email" class="form-control" id="ppEmail" name="ppEmail" placeholder="you@example.com">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppAddress">Address</label>
                                <input type="text" class="form-control" id="ppAddress" name="ppAddress" placeholder="1234 Main St" >
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppAddress2">Address 2 <span class="text-muted">(Optional)</span></label>
                                <input type="text" class="form-control" id="ppAddress2" name="ppAddress2" placeholder="Apartment or suite">            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="ppSuburb">Suburb</label>
                                <input type="text" class="form-control" id="ppSuburb" name="ppSuburb" placeholder="Suburb">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="ppState">State</label>
                                <input type="text" class="form-control" id="ppState" name="ppState" placeholder="State">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="ppPostcode">Postcode</label>
                                <input type="text" class="form-control" id="ppPostcode" name="ppPostcode" placeholder="Postcode" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="ppCountry">Country</label>
                                <input type="text" class="form-control" id="ppCountry" name="ppCountry" placeholder="Country">
                            </div>
                        </div>

                        @endif

                        <hr class="mb-4">

                        <h6>Patient Details:</h6>   

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="patientIdentificationNumber">Patient Identification Number:</label>
                                <input type="text" class="form-control" id="patientIdentificationNumber" name="patientIdentificationNumber" placeholder="Patient Identification Number" value="{{ $report->reportPatientDetail->patient_identification_number }}">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="patientInitials">Patient Initials:</label>
                                <input type="text" class="form-control" id="patientInitials" name="patientInitials" placeholder="Patient Initials" value="{{ $report->reportPatientDetail->patient_initials }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ethnicity">Ethnicity: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityCaucasian" value="Caucasian" @if($report->reportPatientDetail->ethnicity === 'Caucasian') {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ethnicityCaucasian">Caucasian</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityEuropean" value="European" @if($report->reportPatientDetail->ethnicity === 'European') {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ethnicityEuropean">European</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityAsian" value="Asian" @if($report->reportPatientDetail->ethnicity === 'Asian') {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ethnicityAsian">Asian</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityBlack" value="Black" @if($report->reportPatientDetail->ethnicity === 'Black') {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ethnicityBlack">Black</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityIndian" value="Indian" @if($report->reportPatientDetail->ethnicity === 'Indian') {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ethnicityIndian">Indian</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityMiddleEastern" value="Middle Eastern" @if($report->reportPatientDetail->ethnicity === 'Middle Eastern') {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ethnicityMiddleEastern">Middle Eastern</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityAboriginal" value="Aboriginal/Torrens Strait" @if($report->reportPatientDetail->ethnicity === 'Aboriginal/Torrens Strait') {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ethnicityAboriginal">Aboriginal/Torrens Strait</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityPolynesian" value="Polynesian" @if($report->reportPatientDetail->ethnicity === 'Polynesian') {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="ethnicityPolynesian">Polynesian</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="sex">Sex: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sex" id="Male" value="Male" @if($report->reportPatientDetail->sex === 'Male') {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="Male">Male</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sex" id="Female" value="Female" @if($report->reportPatientDetail->sex === 'Female') {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="Female">Female</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="dateOfBirth">Date of Birth:</label>
                                <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth" placeholder="Date of Birth (DD/MM/YYYY)" value="{{ $report->reportPatientDetail->date_of_birth }}">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="weight">Weight (Kg):</label>
                                <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight" value="{{ $report->reportPatientDetail->weight }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="symptoms">Patient symptoms prior prescription:</label>
                                <textarea rows="3" class="form-control" id="symptoms" name="symptoms" placeholder="Symptoms" >{{ $report->reportPatientDetail->patient_symptoms }}</textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="diagnosis">Patient diagnosis resulting in prescription:</label>
                                <textarea rows="3" class="form-control" id="diagnosis" name="diagnosis" placeholder="Diagnosis" >{{ $report->reportPatientDetail->patient_diagnosis }}</textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="genetic">Hereditary or genetic factors noteworthy for this report:</label>
                                <textarea rows="3" class="form-control" id="genetic" name="genetic" placeholder="Hereditary or genetic factors..." >{{ $report->reportPatientDetail->genetic_factors }}</textarea>
                            </div>
                        </div>      

                        <hr class="mb-4">

                        <h6>Vitals of Patient worth noting for this report:</h6>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="patientWeight">Weight:</label>
                                <input type="text" class="form-control" id="patientWeight" name="patientWeight" placeholder="Weight" value="{{ $report->reportPatientDetail->vitals_weight }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientTemp">Temp:</label>
                                <input type="text" class="form-control" id="patientTemp" name="patientTemp" placeholder="Temperature" value="{{ $report->reportPatientDetail->vitals_temp }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientBP">BP:</label>
                                <input type="text" class="form-control" id="patientBP" name="patientBP" placeholder="Blood Pressure" value="{{ $report->reportPatientDetail->vitals_bp }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientHR">HR:</label>
                                <input type="text" class="form-control" id="patientHR" name="patientHR" placeholder="Heart Rate" value="{{ $report->reportPatientDetail->vitals_hr }}">
                            </div>
                        </div>  

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="patientRespiratoryRate">Respiratory Rate:</label>
                                <input type="text" class="form-control" id="patientRespiratoryRate" name="patientRespiratoryRate" placeholder="Respiratory Rate" value="{{ $report->reportPatientDetail->vitals_respiratory_rate }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientOxygenation">Oxygenation:</label>
                                <input type="text" class="form-control" id="patientOxygenation" name="patientOxygenation" placeholder="Oxygenation" value="{{ $report->reportPatientDetail->vitals_oxygenation }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientPain">Pain:</label>
                                <input type="text" class="form-control" id="patientPain" name="patientPain" placeholder="Pain" value="{{ $report->reportPatientDetail->vitals_pain }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientAppetite">Appetite:</label>
                                <input type="text" class="form-control" id="patientAppetite" name="patientAppetite" placeholder="Appetite" value="{{ $report->reportPatientDetail->vitals_appetite }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="patientUrination">Urination:</label>
                                <input type="text" class="form-control" id="patientUrination" name="patientUrination" placeholder="Urination" value="{{ $report->reportPatientDetail->vitals_urination }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientDefecation">Defecation:</label>
                                <input type="text" class="form-control" id="patientDefecation" name="patientDefecation" placeholder="Defecation" value="{{ $report->reportPatientDetail->vitals_defecation }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientVomiting">Vomiting:</label>
                                <input type="text" class="form-control" id="patientVomiting" name="patientVomiting" placeholder="Vomiting" value="{{ $report->reportPatientDetail->vitals_vomiting }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientWaterIntake">Water Intake (mL):</label>
                                <input type="text" class="form-control" id="patientWaterIntake" name="patientWaterIntake" placeholder="Water Intake (mL)" value="{{ $report->reportPatientDetail->vitals_water_intake }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="patientFoodIntake">Food Intake:</label>
                                <input type="text" class="form-control" id="patientFoodIntake" name="patientFoodIntake" placeholder="Food Intake" value="{{ $report->reportPatientDetail->vitals_food_intake }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientLastMedicationTime">Last Medication Time:</label>
                                <input type="text" class="form-control" id="patientLastMedicationTime" name="patientLastMedicationTime" placeholder="Last Medication Time" value="{{ $report->reportPatientDetail->vitals_last_medication_time }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientLastMedicationDosage">Last Medication Dosage:</label>
                                <input type="text" class="form-control" id="patientLastMedicationDosage" name="patientLastMedicationDosage" placeholder="Last Medication Dosage" value="{{ $report->reportPatientDetail->vitals_last_medication_dosage }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientLastMedicationName">Last Medication Name:</label>
                                <input type="text" class="form-control" id="patientLastMedicationName" name="patientLastMedicationName" placeholder="Last Medication Name" value="{{ $report->reportPatientDetail->vitals_last_medication_name }}">
                            </div>
                        </div>

                        <div class="row">                 
                            <div class="col-md-12 mb-3">
                                <label for="pathology">Pathology to support diagnosis, please describe in detail or upload report:</label>
                                <textarea rows="3" class="form-control" id="pathology" name="pathology" placeholder="Pathology to support diagnosis..." >{{ $report->reportPatientDetail->pathology }}</textarea>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="pathologyFile" name="pathologyFile">
                                    <label class="custom-file-label" for="pathologyFile">Choose file</label>
                                </div>
                            </div>
                        </div>

                        <hr class="mb-4">

                        <h6>Suspected medicine(s):</h6>

                        <div class="form-group dynamic-medicine-element" style="display:none">
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <!-- <label for="formulation">Formulation in question:</label> -->
                                    <input type="text" name="formulation[]" placeholder="Formulation in question" class="form-control" /> 
                                </div>
                                <div class="col-md-2 mb-3">
                                    <!-- <label for="dosage">Dosage:</label> -->
                                    <input type="text" name="dosage[]" placeholder="Dosage" class="form-control" /> 
                                </div>
                                <div class="col-md-3 mb-3">
                                    <!-- <label for="dateBegun">Date Begun:</label> -->
                                    <input type="text" name="dateBegun[]" placeholder="Date Begun" class="form-control" /> 
                                </div>
                                <div class="col-md-3 mb-3">
                                    <!-- <label for="patientRespiratoryRate">Date Stopped:</label> -->
                                    <input type="text" name="dateStopped[]" placeholder="Date Stopped" class="form-control" /> 
                                </div>
                                <div class="col-md-1 mb-1">
                                    <button type="button" name="delete" id="delete" class="btn btn-danger add-one-medicine delete">X</button>
                                </div>
                            </div>
                        </div>

                        <div class="dynamic-medicine">
                            @foreach ($report->reportMedicineDetail as $medicine)
                            <div class="form-group dynamic-medicine-element">
                                <div class="row">
                                    <div class="col-md-3 mb-3">
                                        <!-- <label for="formulation">Formulation in question:</label> -->
                                        <input type="text" name="formulation[]" placeholder="Formulation in question" class="form-control" value="{{ $medicine->formulation }}" /> 
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <!-- <label for="dosage">Dosage:</label> -->
                                        <input type="text" name="dosage[]" placeholder="Dosage" class="form-control" value="{{ $medicine->dosage }}" /> 
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <!-- <label for="dateBegun">Date Begun:</label> -->
                                        <input type="text" name="dateBegun[]" placeholder="Date Begun" class="form-control" value="{{ $medicine->date_begun }}" /> 
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <!-- <label for="patientRespiratoryRate">Date Stopped:</label> -->
                                        <input type="text" name="dateStopped[]" placeholder="Date Stopped" class="form-control" value="{{ $medicine->date_stopped }}" /> 
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <button type="button" name="delete" id="delete" class="btn btn-danger add-one-medicine delete">X</button>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <button type="button" name="add" id="add" class="btn btn-success add-one-medicine">Add Medicine</button>

                        <hr class="mb-4">

                        <h6>Other medicine(s) taken at the time of the reaction:</h6>
                        <h6>Reaction(s):</h6>

                        <div class="row"> 
                            <div class="col-md-6 mb-3">
                                <label for="dateofOnset">Date of Onset:</label>
                                <input type="text" class="form-control" id="dateofOnset" name="dateofOnset" placeholder="Date of Onset" value="{{ $report->reportReactionDetail->date_of_onset }}" >
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lengthofReaction">Length of Reaction:</label>
                                <input type="text" class="form-control" id="lengthofReaction" name="lengthofReaction" placeholder="Length of Reaction" value="{{ $report->reportReactionDetail->length_of_reaction }}" >
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-6 mb-3">
                                <label for="reactionPhotoFile">Upload Photo:</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="reactionPhotoFile" name="reactionPhotoFile">
                                    <label class="custom-file-label" for="reactionPhotoFile">Choose file</label>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="reactionPathologyFile">Upload Pathology:</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="reactionPathologyFile" name="reactionPathologyFile">
                                    <label class="custom-file-label" for="reactionPathologyFile">Choose file</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">                         
                            <div class="col-md-12 mb-3">
                                <label for="reactionDescription">Describe reaction in as much details as possible:</label>
                                <textarea rows="3" class="form-control" id="reactionDescription" name="reactionDescription" placeholder="Describe reaction in as much details as possible..." >{{ $report->reportReactionDetail->reaction_description }}</textarea>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <label for="diet">Has patient kept a current diet diary or has undertaken a specialised diet: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="diet" id="dietYes" value="1" @if($report->reportReactionDetail->specialised_diet === 1) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="dietYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="diet" id="dietNo" value="0" @if($report->reportReactionDetail->specialised_diet === 0) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="dietNo">No</label>
                                </div>
                            </div>
                        </div>

                        <h6>Seriousness of Reaction:</h6>

                        <div class="row"> 
                            <div class="col-md-4 mb-3">
                                <label for="lifeThreatening">Life Threatening:</label>
                                <input type="text" class="form-control" id="lifeThreatening" name="lifeThreatening" placeholder="Life Threatening" value="{{ $report->reportReactionDetail->life_threatening }}" >
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="medicalIntervention">Medical Intervention:</label>
                                <input type="text" class="form-control" id="medicalIntervention" name="medicalIntervention" placeholder="Medical Intervention" value="{{ $report->reportReactionDetail->medical_intervention }}" >
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="hospitalised">Hospitalised:</label>
                                <input type="text" class="form-control" id="hospitalised" name="hospitalised" placeholder="Hospitalised" value="{{ $report->reportReactionDetail->hospitalised }}" >
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3"> 
                                <label for="reactionOther">Other:</label>
                                <textarea rows="3" class="form-control" id="reactionOther" name="reactionOther" placeholder="" >{{ $report->reportReactionDetail->other }}</textarea>
                            </div>
                        </div>

                        <h6>Outcome:</h6>

                        <div class="row"> 
                            <div class="col-md-4 mb-3">
                                <label for="outcomeFullRecovery">Full Recovery:</label>
                                <input type="text" class="form-control" id="outcomeFullRecovery" name="outcomeFullRecovery" placeholder="Full Recovery" value="{{ $report->reportReactionDetail->full_recovery }}" >
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="outcomeUnknown">Unknown:</label>
                                <input type="text" class="form-control" id="outcomeUnknown" name="outcomeUnknown" placeholder="Unknown" value="{{ $report->reportReactionDetail->unknown }}" >
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="outcomeRecovering">Recovering but not yet fully recovered:</label>
                                <input type="text" class="form-control" id="outcomeRecovering" name="outcomeRecovering" placeholder="Recovering" value="{{ $report->reportReactionDetail->recovering }}" >
                            </div>
                        </div>

                        <hr class="mb-4">

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <label for="sequelae">Sequelae: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sequelae" id="sequelaeYes" value="1" @if($report->reportOtherDetail->sequelae === 1) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="sequelaeYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sequelae" id="sequelaeNo" value="0" @if($report->reportOtherDetail->sequelae === 0) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="sequelaeNo">No</label>
                                </div>
                            </div>
                        </div>

                        <h6>If Yes, describe:</h6>

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <label for="rechallenged">Was medication and dosage rechallenged: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="rechallenged" id="rechallengedYes" value="1" @if($report->reportOtherDetail->rechallenged === 1) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="rechallengedYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="rechallenged" id="rechallengedNo" value="0" @if($report->reportOtherDetail->rechallenged === 0) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="rechallengedNo">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3"> 
                                <label for="outcomes">If Yes, what were the outcomes?</label>
                                <textarea rows="3" class="form-control" id="outcomes" name="outcomes" placeholder="" >{{ $report->reportOtherDetail->outcomes }}</textarea>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3"> 
                                <label for="recovered">Was reaction recovered?</label>
                                <input type="text" class="form-control" id="recovered" name="recovered" placeholder="Was reaction recovered?" value="{{ $report->reportOtherDetail->recovered }}" >
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <label for="postmortem">In the event of death, is there a post mortem report: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="postmortem" id="postmortemYes" value="1" @if($report->reportOtherDetail->post_mortem_report === 1) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="postmortemYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="postmortem" id="postmortemNo" value="0"  @if($report->reportOtherDetail->post_mortem_report === 0) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="postmortemNo">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <label for="causeofDeath">Is the cause of death known: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="deathKnown" id="deathKnownYes" value="1" @if($report->reportOtherDetail->death_known == 1) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="deathKnownYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="deathKnown" id="deathKnownNo" value="0" @if($report->reportOtherDetail->death_known == 0) {{ 'checked' }} @endif>
                                    <label class="form-check-label" for="deathKnownNo">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3"> 
                                <label for="causeofDeath">If Yes, what?</label>
                                <input type="text" class="form-control" id="causeofDeath" name="causeofDeath" placeholder="Cause of Death" value="{{ $report->reportOtherDetail->cause_of_death }}" >
                            </div>
                        </div>

                        <hr class="mb-4">

                        <div class="row"> 
                            <div class="col-md-12 mb-3">                                
                                <label for="lastName">Who is making this report?</label>
                                <input type="text" class="form-control" id="addedBy" name="addedBy" placeholder="" value="{{ $report->added_by }}" >
                            </div>
                        </div>

                        <!-- <h6>Terms of Use:</h6>

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="invalidCheck" >
                                    <label class="form-check-label" for="invalidCheck">
                                        By clicking here you state that the information entered is true and correct, and that in reporting this suspected adverse reaction, you are requesting Medlab to investigate the matter further.
                                    </label>
                                </div>
                            </div>
                        </div> -->

                        <button class="btn btn-info" type="submit">Update Report</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.custom-file-input').on('change', function() { 
        let fileName = $(this).val().split('\\').pop(); 
        $(this).next('.custom-file-label').addClass("selected").html(fileName); 
    });

    // Clone the hidden element and shows it
    $('.add-one-medicine').click(function(){
        $('.dynamic-medicine-element').first().clone().appendTo('.dynamic-medicine').show();
        attach_delete();
    });

    //Attach functionality to delete buttons
    function attach_delete(){
        $('.delete').off();
        $('.delete').click(function(){
            $(this).closest('.form-group').remove();
        });
    }
</script>
@endsection