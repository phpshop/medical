<?php

namespace App\Http\Controllers;

use App\Report\Report;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->report = new Report();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if (Auth::user()->isAdmin() ) {
            $reports = $this->report->get();
            return view('admin.home', [
                'reports' => $reports
            ]);
        }
        else {
            $userId = Auth::id();
            $reports = $this->report->where('user_id', '=', $userId)->get();
            return view('home', [
                'reports' => $reports
            ]);
        }
    }
    
}
