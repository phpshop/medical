<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

use File;

class ReportPatientDetail extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'report_patient_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'report_id', 
        'patient_identification_number', 
        'patient_initials', 
        'ethnicity', 
        'sex', 
        'date_of_birth', 
        'weight', 
        'patient_symptoms', 
        'patient_diagnosis', 
        'genetic_factors', 
        'vitals_weight', 
        'vitals_temp', 
        'vitals_bp', 
        'vitals_hr', 
        'vitals_respiratory_rate', 
        'vitals_oxygenation', 
        'vitals_pain', 
        'vitals_appetite', 
        'vitals_urination', 
        'vitals_defecation', 
        'vitals_vomiting', 
        'vitals_water_intake', 
        'vitals_food_intake', 
        'vitals_last_medication_time', 
        'vitals_last_medication_dosage', 
        'vitals_last_medication_name', 
        'pathology', 
        'pathology_file'
    ];

    public function uploadFile($newFile, $reportNumber)
    {
        if (!File::exists("uploads/reports")) :
            File::makeDirectory("uploads/reports", 0775, true);
        endif;

        $unique = substr(sha1(rand(1, 100000).date("c")), 0, 20);
        $fileName = $reportNumber.'_'.$unique;

        /* upload file */
        $extensionFile = $newFile->getClientOriginalExtension(); 
        $fileNameWithExtension = $fileName.'.'.$extensionFile;

        $newFile->move("uploads/reports", $fileNameWithExtension); 
        return $fileNameWithExtension;
    }

}
