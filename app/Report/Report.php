<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reports';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'report_number',
        'added_by',
        'user_id',
    ];

    public function reportPractitionerDetail()
    {
        return $this->hasOne('App\Report\ReportPractitionerDetail', 'report_id', 'id');
    }

    public function reportPrimaryPractitionerDetail()
    {
        return $this->hasOne('App\Report\ReportPrimaryPractitionerDetail', 'report_id', 'id');
    }

    public function reportPatientDetail()
    {
        return $this->hasOne('App\Report\ReportPatientDetail', 'report_id', 'id');
    }

    public function reportMedicineDetail()
    {
        return $this->hasMany('App\Report\ReportMedicineDetail', 'report_id', 'id');
    }

    public function reportReactionDetail()
    {
        return $this->hasOne('App\Report\ReportReactionDetail', 'report_id', 'id');
    }

    public function reportOtherDetail()
    {
        return $this->hasOne('App\Report\ReportOtherDetail', 'report_id', 'id');
    }

}
