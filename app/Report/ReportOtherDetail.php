<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

class ReportOtherDetail extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'report_other_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'report_id', 
        'sequelae', 
        'rechallenged', 
        'outcomes', 
        'recovered', 
        'post_mortem_report', 
        'death_known', 
        'cause_of_death'
    ];

}
