@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">Echelon Incident Report</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!-- You are logged in! -->

                    <form action="{{URL::to('/incident-form-post')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <!-- <div class="form-group">
                            Incident Report Number: AUTO GEN 
                        </div> -->
                        <div class="form-group">
                            Author’s Name: {{ Auth::user()->name }}
                            <!-- <input type="hidden" name="addedBy" value="{{ Auth::user()->name }}"> -->
                        </div>

                        <h6>Practitioner Details:</h6>

                        <div class="row">
                            <div class="col-md-2 mb-3">
                                <label for="title">Title</label>
                                <select class="form-control" id="title" name="title" >
                                    <option value="">Choose...</option>
                                    <option>Mr.</option>
                                    <option>Mrs.</option>
                                    <option>Miss</option>
                                    <option>Ms.</option>
                                    <option>Dr.</option>
                                    <option>Prof.</option>
                                    <option>Rev.</option>
                                    <option>Other</option>
                                </select>
                                <div class="invalid-feedback">
                                Please select a valid tittle.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="firstName">First name</label>
                                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" value="" >
                                <div class="invalid-feedback">
                                Valid first name is required.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="lastName">Last name</label>
                                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" value="" >
                                <div class="invalid-feedback">
                                Valid last name is required.
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="medicalClinic">Medical Clinic: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="medicalClinic" id="medicalClinicYes" value="1">
                                    <label class="form-check-label" for="medicalClinicYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="medicalClinic" id="medicalClinicNo" value="0">
                                    <label class="form-check-label" for="medicalClinicNo">No</label>
                                </div>
                                <input type="text" class="form-control" id="medicalClinicName" name="medicalClinicName" placeholder="Medical Clinic Name" value="" >
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="hospital">Hospital: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="hospital" id="hospitalYes" value="1">
                                    <label class="form-check-label" for="hospitalYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="hospital" id="hospitalNo" value="0">
                                    <label class="form-check-label" for="hospitalNo">No</label>
                                </div>
                                <input type="text" class="form-control" id="hospitalName" name="hospitalName" placeholder="Hospital Name" value="" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="hospitalUnitDetails">If Hospital, Unit Details: </label>
                                <textarea class="form-control" id="hospitalUnitDetails" name="hospitalUnitDetails" placeholder="Hospital Unit Details" ></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St" >
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
                                <input type="text" class="form-control" id="address2" name="address2" placeholder="Apartment or suite">            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="suburb">Suburb</label>
                                <input type="text" class="form-control" id="suburb" name="suburb" placeholder="Suburb">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="state">State</label>
                                <input type="text" class="form-control" id="state" name="state" placeholder="State">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="postcode">Postcode</label>
                                <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="country">Country</label>
                                <input type="text" class="form-control" id="country" name="country" placeholder="Country">
                            </div>
                        </div>

                        <hr class="mb-4">
                        
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="isPrimaryPractitioner">Is Primary Practitioner Known: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="isPrimaryPractitioner" id="primaryPractitionerYes" value="1" required>
                                    <label class="form-check-label" for="primaryPractitionerYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="isPrimaryPractitioner" id="primaryPractitionerNo" value="0">
                                    <label class="form-check-label" for="primaryPractitionerNo">No</label>
                                </div>
                            </div>
                        </div>

                        <h6>If Yes, then Primary Practitioner Details:</h6>   

                        <div class="row">
                        <div class="col-md-5 mb-3">
                            <label for="">If same as above select here </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input position-static" type="checkbox" id="sameAsPractitioner" value="Yes" aria-label="...">
                            </div>
                        </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2 mb-3">
                                <label for="title">Title</label>
                                <select class="form-control" id="ppTitle" name="ppTitle" >
                                    <option value="">Choose...</option>
                                    <option>Mr.</option>
                                    <option>Mrs.</option>
                                    <option>Miss</option>
                                    <option>Ms.</option>
                                    <option>Dr.</option>
                                    <option>Prof.</option>
                                    <option>Rev.</option>
                                    <option>Other</option>
                                </select>
                                <div class="invalid-feedback">
                                Please select a valid tittle.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="firstName">First name</label>
                                <input type="text" class="form-control" id="ppFirstName" name="ppFirstName" placeholder="First Name" value="" >
                                <div class="invalid-feedback">
                                Valid first name is required.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="lastName">Last name</label>
                                <input type="text" class="form-control" id="ppLastName" name="ppLastName" placeholder="Last Name" value="" >
                                <div class="invalid-feedback">
                                Valid last name is required.
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="ppMedicalClinic">Medical Clinic: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppMedicalClinic" id="ppMedicalClinicYes" value="1">
                                    <label class="form-check-label" for="ppMedicalClinicYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppMedicalClinic" id="ppMedicalClinicNo" value="0">
                                    <label class="form-check-label" for="ppMedicalClinicNo">No</label>
                                </div>
                                <input type="text" class="form-control" id="ppMedicalClinicName" name="ppMedicalClinicName" placeholder="Medical Clinic Name" value="" >
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="ppHospital">Hospital: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppHospital" id="ppHospitalYes" value="1">
                                    <label class="form-check-label" for="ppHospitalYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ppHospital" id="ppHospitalNo" value="0">
                                    <label class="form-check-label" for="ppHospitalNo">No</label>
                                </div>
                                <input type="text" class="form-control" id="ppHospitalName" name="ppHospitalName" placeholder="Hospital Name" value="" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppHospitalUnitDetails">If Hospital, Unit Details: </label>
                                <textarea class="form-control" id="ppHospitalUnitDetails" name="ppHospitalUnitDetails" placeholder="Hospital Unit Details" ></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppEmail">Email</label>
                                <input type="email" class="form-control" id="ppEmail" name="ppEmail" placeholder="you@example.com">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppAddress">Address</label>
                                <input type="text" class="form-control" id="ppAddress" name="ppAddress" placeholder="1234 Main St" >
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ppAddress2">Address 2 <span class="text-muted">(Optional)</span></label>
                                <input type="text" class="form-control" id="ppAddress2" name="ppAddress2" placeholder="Apartment or suite">            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="ppSuburb">Suburb</label>
                                <input type="text" class="form-control" id="ppSuburb" name="ppSuburb" placeholder="Suburb">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="ppState">State</label>
                                <input type="text" class="form-control" id="ppState" name="ppState" placeholder="State">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="ppPostcode">Postcode</label>
                                <input type="text" class="form-control" id="ppPostcode" name="ppPostcode" placeholder="Postcode" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="ppCountry">Country</label>
                                <input type="text" class="form-control" id="ppCountry" name="ppCountry" placeholder="Country">
                            </div>
                        </div>

                        <hr class="mb-4">

                        <h6>Patient Details:</h6>   

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="patientIdentificationNumber">Patient Identification Number:</label>
                                <input type="text" class="form-control" id="patientIdentificationNumber" name="patientIdentificationNumber" placeholder="Patient Identification Number" >
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="patientInitials">Patient Initials:</label>
                                <input type="text" class="form-control" id="patientInitials" name="patientInitials" placeholder="Patient Initials" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="ethnicity">Ethnicity: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityCaucasian" value="Caucasian">
                                    <label class="form-check-label" for="ethnicityCaucasian">Caucasian</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityEuropean" value="European">
                                    <label class="form-check-label" for="ethnicityEuropean">European</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityAsian" value="Asian">
                                    <label class="form-check-label" for="ethnicityAsian">Asian</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityBlack" value="Black">
                                    <label class="form-check-label" for="ethnicityBlack">Black</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityIndian" value="Indian">
                                    <label class="form-check-label" for="ethnicityIndian">Indian</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityMiddleEastern" value="Middle Eastern">
                                    <label class="form-check-label" for="ethnicityMiddleEastern">Middle Eastern</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityAboriginal" value="Aboriginal/Torrens Strait">
                                    <label class="form-check-label" for="ethnicityAboriginal">Aboriginal/Torrens Strait</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="ethnicity" id="ethnicityPolynesian" value="Polynesian">
                                    <label class="form-check-label" for="ethnicityPolynesian">Polynesian</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="sex">Sex: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sex" id="Male" value="Male">
                                    <label class="form-check-label" for="Male">Male</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sex" id="Female" value="Female">
                                    <label class="form-check-label" for="Female">Female</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="dateOfBirth">Date of Birth:</label>
                                <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth" placeholder="Date of Birth (DD/MM/YYYY)" >
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="weight">Weight (Kg):</label>
                                <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="symptoms">Patient symptoms prior prescription:</label>
                                <textarea rows="3" class="form-control" id="symptoms" name="symptoms" placeholder="Symptoms" ></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="diagnosis">Patient diagnosis resulting in prescription:</label>
                                <textarea rows="3" class="form-control" id="diagnosis" name="diagnosis" placeholder="Diagnosis" ></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="genetic">Hereditary or genetic factors noteworthy for this report:</label>
                                <textarea rows="3" class="form-control" id="genetic" name="genetic" placeholder="Hereditary or genetic factors..." ></textarea>
                            </div>
                        </div>      

                        <hr class="mb-4">

                        <h6>Vitals of Patient worth noting for this report:</h6>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="patientWeight">Weight:</label>
                                <input type="text" class="form-control" id="patientWeight" name="patientWeight" placeholder="Weight" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientTemp">Temp:</label>
                                <input type="text" class="form-control" id="patientTemp" name="patientTemp" placeholder="Temperature" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientBP">BP:</label>
                                <input type="text" class="form-control" id="patientBP" name="patientBP" placeholder="Blood Pressure" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientHR">HR:</label>
                                <input type="text" class="form-control" id="patientHR" name="patientHR" placeholder="Heart Rate" >
                            </div>
                        </div>  

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="patientRespiratoryRate">Respiratory Rate:</label>
                                <input type="text" class="form-control" id="patientRespiratoryRate" name="patientRespiratoryRate" placeholder="Respiratory Rate" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientOxygenation">Oxygenation:</label>
                                <input type="text" class="form-control" id="patientOxygenation" name="patientOxygenation" placeholder="Oxygenation" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientPain">Pain:</label>
                                <input type="text" class="form-control" id="patientPain" name="patientPain" placeholder="Pain" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientAppetite">Appetite:</label>
                                <input type="text" class="form-control" id="patientAppetite" name="patientAppetite" placeholder="Appetite" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="patientUrination">Urination:</label>
                                <input type="text" class="form-control" id="patientUrination" name="patientUrination" placeholder="Urination" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientDefecation">Defecation:</label>
                                <input type="text" class="form-control" id="patientDefecation" name="patientDefecation" placeholder="Defecation" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientVomiting">Vomiting:</label>
                                <input type="text" class="form-control" id="patientVomiting" name="patientVomiting" placeholder="Vomiting" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientWaterIntake">Water Intake (mL):</label>
                                <input type="text" class="form-control" id="patientWaterIntake" name="patientWaterIntake" placeholder="Water Intake (mL)" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="patientFoodIntake">Food Intake:</label>
                                <input type="text" class="form-control" id="patientFoodIntake" name="patientFoodIntake" placeholder="Food Intake" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientLastMedicationTime">Last Medication Time:</label>
                                <input type="text" class="form-control" id="patientLastMedicationTime" name="patientLastMedicationTime" placeholder="Last Medication Time" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientLastMedicationDosage">Last Medication Dosage:</label>
                                <input type="text" class="form-control" id="patientLastMedicationDosage" name="patientLastMedicationDosage" placeholder="Last Medication Dosage" >
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="patientLastMedicationName">Last Medication Name:</label>
                                <input type="text" class="form-control" id="patientLastMedicationName" name="patientLastMedicationName" placeholder="Last Medication Name" >
                            </div>
                        </div>

                        <div class="row">                 
                            <div class="col-md-12 mb-3">
                                <label for="pathology">Pathology to support diagnosis, please describe in detail or upload report:</label>
                                <textarea rows="3" class="form-control" id="pathology" name="pathology" placeholder="Pathology to support diagnosis..." ></textarea>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="pathologyFile" name="pathologyFile">
                                    <label class="custom-file-label" for="pathologyFile">Choose file</label>
                                </div>
                            </div>
                        </div>

                        <hr class="mb-4">

                        <h6>Suspected medicine(s):</h6>

                        <div class="form-group dynamic-medicine-element" style="display:none">
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <!-- <label for="formulation">Formulation in question:</label> -->
                                    <input type="text" name="formulation[]" placeholder="Formulation in question" class="form-control" /> 
                                </div>
                                <div class="col-md-2 mb-3">
                                    <!-- <label for="dosage">Dosage:</label> -->
                                    <input type="text" name="dosage[]" placeholder="Dosage" class="form-control" /> 
                                </div>
                                <div class="col-md-3 mb-3">
                                    <!-- <label for="dateBegun">Date Begun:</label> -->
                                    <input type="text" name="dateBegun[]" placeholder="Date Begun" class="form-control" /> 
                                </div>
                                <div class="col-md-3 mb-3">
                                    <!-- <label for="patientRespiratoryRate">Date Stopped:</label> -->
                                    <input type="text" name="dateStopped[]" placeholder="Date Stopped" class="form-control" /> 
                                </div>
                                <div class="col-md-1 mb-1">
                                    <button type="button" name="delete" id="delete" class="btn btn-danger add-one-medicine delete">X</button>
                                </div>
                            </div>
                        </div>

                        <div class="dynamic-medicine">
                        </div>

                        <button type="button" name="add" id="add" class="btn btn-success add-one-medicine">Add Medicine</button>

                        <hr class="mb-4">

                        <h6>Other medicine(s) taken at the time of the reaction:</h6>
                        <h6>Reaction(s):</h6>

                        <div class="row"> 
                            <div class="col-md-6 mb-3">
                                <label for="dateofOnset">Date of Onset:</label>
                                <input type="text" class="form-control" id="dateofOnset" name="dateofOnset" placeholder="Date of Onset" >
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lengthofReaction">Length of Reaction:</label>
                                <input type="text" class="form-control" id="lengthofReaction" name="lengthofReaction" placeholder="Length of Reaction" >
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-6 mb-3">
                                <label for="reactionPhotoFile">Upload Photo:</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="reactionPhotoFile" name="reactionPhotoFile">
                                    <label class="custom-file-label" for="reactionPhotoFile">Choose file</label>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="reactionPathologyFile">Upload Pathology:</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="reactionPathologyFile" name="reactionPathologyFile">
                                    <label class="custom-file-label" for="reactionPathologyFile">Choose file</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">                         
                            <div class="col-md-12 mb-3">
                                <label for="reactionDescription">Describe reaction in as much details as possible:</label>
                                <textarea rows="3" class="form-control" id="reactionDescription" name="reactionDescription" placeholder="Describe reaction in as much details as possible..." ></textarea>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <label for="diet">Has patient kept a current diet diary or has undertaken a specialised diet: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="diet" id="dietYes" value="1">
                                    <label class="form-check-label" for="dietYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="diet" id="dietNo" value="0">
                                    <label class="form-check-label" for="dietNo">No</label>
                                </div>
                            </div>
                        </div>

                        <h6>Seriousness of Reaction:</h6>

                        <div class="row"> 
                            <div class="col-md-4 mb-3">
                                <label for="lifeThreatening">Life Threatening:</label>
                                <input type="text" class="form-control" id="lifeThreatening" name="lifeThreatening" placeholder="Life Threatening" >
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="medicalIntervention">Medical Intervention:</label>
                                <input type="text" class="form-control" id="medicalIntervention" name="medicalIntervention" placeholder="Medical Intervention" >
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="hospitalised">Hospitalised:</label>
                                <input type="text" class="form-control" id="hospitalised" name="hospitalised" placeholder="Hospitalised" >
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3"> 
                                <label for="reactionOther">Other:</label>
                                <textarea rows="3" class="form-control" id="reactionOther" name="reactionOther" placeholder="" ></textarea>
                            </div>
                        </div>
                        
                        <h6>Outcome:</h6>

                         <div class="row"> 
                            <div class="col-md-4 mb-3">
                                <label for="outcomeFullRecovery">Full Recovery:</label>
                                <input type="text" class="form-control" id="outcomeFullRecovery" name="outcomeFullRecovery" placeholder="Full Recovery" >
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="outcomeUnknown">Unknown:</label>
                                <input type="text" class="form-control" id="outcomeUnknown" name="outcomeUnknown" placeholder="Unknown" >
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="outcomeRecovering">Recovering but not yet fully recovered:</label>
                                <input type="text" class="form-control" id="outcomeRecovering" name="outcomeRecovering" placeholder="Recovering" >
                            </div>
                        </div>

                        <hr class="mb-4">

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <label for="sequelae">Sequelae: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sequelae" id="sequelaeYes" value="1">
                                    <label class="form-check-label" for="sequelaeYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sequelae" id="sequelaeNo" value="0">
                                    <label class="form-check-label" for="sequelaeNo">No</label>
                                </div>
                            </div>
                        </div>

                        <h6>If Yes, describe:</h6>

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <label for="rechallenged">Was medication and dosage rechallenged: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="rechallenged" id="rechallengedYes" value="1">
                                    <label class="form-check-label" for="rechallengedYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="rechallenged" id="rechallengedNo" value="0">
                                    <label class="form-check-label" for="rechallengedNo">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3"> 
                                <label for="outcomes">If Yes, what were the outcomes?</label>
                                <textarea rows="3" class="form-control" id="outcomes" name="outcomes" placeholder="" ></textarea>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3"> 
                                <label for="recovered">Was reaction recovered?</label>
                                <input type="text" class="form-control" id="recovered" name="recovered" placeholder="Was reaction recovered?" >
                            </div>
                        </div>
                        
                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <label for="postmortem">In the event of death, is there a post mortem report: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="postmortem" id="postmortemYes" value="1">
                                    <label class="form-check-label" for="postmortemYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="postmortem" id="postmortemNo" value="0">
                                    <label class="form-check-label" for="postmortemNo">No</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <label for="causeofDeath">Is the cause of death known: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="deathKnown" id="deathKnownYes" value="1">
                                    <label class="form-check-label" for="deathKnownYes">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="deathKnown" id="deathKnownNo" value="0">
                                    <label class="form-check-label" for="deathKnownNo">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="row"> 
                            <div class="col-md-12 mb-3"> 
                                <label for="causeofDeath">If Yes, what?</label>
                                <input type="text" class="form-control" id="causeofDeath" name="causeofDeath" placeholder="Cause of Death" >
                            </div>
                        </div>

                        <hr class="mb-4">

                        <div class="row"> 
                            <div class="col-md-12 mb-3">                                
                                <label for="lastName">Who is making this report?</label>
                                <input type="text" class="form-control" id="addedBy" name="addedBy" placeholder="" value="{{ Auth::user()->name }}" >
                            </div>
                        </div>

                        <hr class="mb-4">

                        <h6>Terms of Use:</h6>

                        <div class="row"> 
                            <div class="col-md-12 mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                    <label class="form-check-label" for="invalidCheck">
                                        By clicking here you state that the information entered is true and correct, and that in reporting this suspected adverse reaction, you are requesting Medlab to investigate the matter further.
                                    </label>
                                </div>
                            </div>
                        </div>

                        <hr class="mb-4">

                        <button class="btn btn-info" type="submit">Submit Report</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.custom-file-input').on('change', function() { 
        let fileName = $(this).val().split('\\').pop(); 
        $(this).next('.custom-file-label').addClass("selected").html(fileName); 
    });

    // Clone the hidden element and shows it
    $('.add-one-medicine').click(function(){
        $('.dynamic-medicine-element').first().clone().appendTo('.dynamic-medicine').show();
        attach_delete();
    });

    //Attach functionality to delete buttons
    function attach_delete(){
        $('.delete').off();
        $('.delete').click(function(){
            $(this).closest('.form-group').remove();
        });
    }
</script>
@endsection

