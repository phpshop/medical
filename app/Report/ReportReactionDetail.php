<?php

namespace App\Report;

use Illuminate\Database\Eloquent\Model;

use File;

class ReportReactionDetail extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'report_reaction_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'report_id', 
        'date_of_onset', 
        'length_of_reaction', 
        'photo', 
        'pathology_file', 
        'reaction_description', 
        'specialised_diet', 
        'life_threatening', 
        'medical_intervention', 
        'hospitalised', 
        'other', 
        'full_recovery', 
        'unknown', 
        'recovering'
    ];

    public function uploadFile($newFile, $reportNumber)
    {
        if (!File::exists("uploads/reports")) :
            File::makeDirectory("uploads/reports", 0775, true);
        endif;

        $unique = substr(sha1(rand(1, 100000).date("c")), 0, 20);
        $fileName = $reportNumber.'_'.$unique;

        /* upload file */
        $extensionFile = $newFile->getClientOriginalExtension(); 
        $fileNameWithExtension = $fileName.'.'.$extensionFile;

        $newFile->move("uploads/reports", $fileNameWithExtension); 
        return $fileNameWithExtension;
    }

}
